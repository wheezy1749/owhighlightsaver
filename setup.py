import sys
import moviepy.video.io.ffmpeg_tools
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os", "numpy", "moviepy"], "excludes": ["tkinter"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None

setup(  name = "owhighlights",
        version = "0.1",
        description = "Save OW highlights automatically",
        options = {"build_exe": build_exe_options},
        executables = [Executable("owhighlights.py", base=base)])
import cv2
import time
import win32api
import win32con
import sys
import os
import argparse
import numpy as np
import imageio
import mss

from pywinauto.keyboard import SendKeys
from pywinauto.findwindows    import find_window
from pywinauto.win32functions import SetForegroundWindow
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
from win32api import GetSystemMetrics
from PIL import Image

imageio.plugins.ffmpeg.download()

# Enum like Classes
class MOVE:
    DOWN  = "{DOWN}"
    UP    = "{UP}"
    LEFT  = "{LEFT}"
    RIGHT = "{RIGHT}"
    TAB   = "{TAB}"
    SPACE = "{SPACE}"
    DELETE = "{DELETE}"
    BACKSPACE = "{BACKSPACE}"
    NAME_VIDEO = "owhiglight_%s" % time.strftime("%m.%d.%Y.%H%M%S", time.gmtime(time.time()))
    ENTER = "{ENTER}"
    ESC   = "{ESC}"
    SELECT_ALL = "^a"
    
class Images:
    PATH = ".\\images\\"
    EXIT_TO_DESKTOP = PATH + "exit_to_desktop.PNG"
    WHITE_FRAME     = PATH + "white_frame.PNG"
    BLACK_FRAME     = PATH + "black_frame.PNG"

# Globals
config_attributes = {'music'         : None,
                     'save_location' : None,
                     'frame_rate'    : None
                    }
                    
def parse_user_input():
    parser = argparse.ArgumentParser(description='Save and trim overwatch highlights automatically')
    parser.add_argument('--top_num', type=int, default=5,
                        help='Number of top 5 videos to save')
    parser.add_argument('--cus_num', type=int, default=10,
                        help='Number of custom videos to save. Max is 50 at a time.')                    
    parser.add_argument('--trim_only', action='store_true',
                        help='Only trim videos in the OW save directory. Skip saving new videos')
    parser.add_argument('--no_trim', action='store_true',
                        help='Do not trim videos intro and outtro after saving videos')

    args = parser.parse_args()
    
    if args.top_num > 5 or args.top_num < 1:
        raise ValueError("top_num value must be between 1 and 5")
    if args.cus_num > 50:
        raise ValueError("cus_num max value is 50")
    if args.cus_num < 0:
        raise ValueError("cus_num must be a positive integer")
        
    return args
        
def get_screen_cap(monitor=None):
    if monitor is None:
        monitor = {'top': 0, 'left': 0, 'width': get_monitor_width(), 'height': get_monitor_height()}
    
    with mss.mss() as sct:
        img = np.array(sct.grab(monitor))
        
    return img

def get_monitor_height():
    return int(GetSystemMetrics(1))

def get_monitor_width():
    return int(GetSystemMetrics(0))

def set_mouse_position(x,y):
    if x > get_monitor_width() or y > get_monitor_height() or x < 0 or y < 0:
        raise ValueError("Mouse position cannot be set outside of monitor range")
    
    x1,y1 = win32api.GetCursorPos()
    win32api.SetCursorPos([x,y])
    
def get_mouse_position():
    return win32api.GetCursorPos()
                
def click(x,y, left=False):
    time.sleep(.1)
    if left:
        set_mouse_position(x,y)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN,x,y,0,0)
        time.sleep(.05)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP,x,y,0,0)
    else:
        set_mouse_position(x,y)
        win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTDOWN,x,y,0,0)
        time.sleep(.05)
        win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTUP,x,y,0,0)
    time.sleep(.1)
    
def compare_images(imageA, imageB):
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])
    
    return err
    
def send_key(key):
    SendKeys(key)
    if "BACKSPACE" not in key:
        time.sleep(.1)
    else:
        time.sleep(.001)
    
def perform_sequence(seq):
    for step in seq:
        if type(step) == type(''):
            send_key(step)
        elif type(step) == type([]):
            for i in range(0, step[0]):
                send_key(step[1])
                       
def wait_for_video():
    time.sleep(8)
    while True:
        imgA = get_screen_cap()
        time.sleep(2)
        imgB = get_screen_cap()
        err = compare_images(imgA, imgB)
        print err
        if  err > 2000:
            print "Done rendering!"
            break
            
def bring_ow_to_front():
    set_mouse_position(0,0)
    try:
        pid = find_window(title='Overwatch')
        SetForegroundWindow(pid)
    except Exception as e:
        raise RuntimeError("Overwatch is not running")
    
def is_sub_image_visible(sub_img): 
    img = get_screen_cap()
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    template = cv2.imread(sub_img,0)
    w, h = template.shape[::-1]
    meth = 'cv2.TM_CCOEFF'
    method = eval(meth)

    # Apply template Matching
    res = cv2.matchTemplate(img, template,method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    top_left = max_loc
    bottom_right = (top_left[0] + w, top_left[1] + h)
    mon = {'top': top_left[1], 'left': top_left[0], 'width': w, 'height': h}
    on_screen = cv2.cvtColor(get_screen_cap(mon), cv2.COLOR_BGR2GRAY)
    err = compare_images(on_screen, template)
    print "Looking for '%s' in screenshot and found it with err = %d" % (sub_img, err)
    return err < 1000
    
def go_to_main_menu():
    esc_count = 10
    
    for i in range(0,esc_count):
        time.sleep(.1)
        SendKeys(MOVE.ESC)
    
    time.sleep(1)
    if is_sub_image_visible(Images.EXIT_TO_DESKTOP):
        SendKeys(MOVE.ESC)
        return
    else:
        SendKeys(MOVE.ESC)
        time.sleep(.5)
        if is_sub_image_visible(Images.EXIT_TO_DESKTOP):
            SendKeys(MOVE.ESC)
        else:
            print "Could not find main menu. Is Overwatch in fullscreen mode? Set to fullscreen windowed mode."
            sys.exit(-1)
        
def go_to_highlights():        
    bring_ow_to_front()
    time.sleep(1)
    go_to_main_menu()
    time.sleep(1)
    
    seq     = [MOVE.TAB, [5, MOVE.DOWN], MOVE.SPACE, MOVE.DOWN]
    perform_sequence(seq)
    
def get_top_num_videos():
    return args.top_num

def get_cus_numtom_videos():
    return args.cus_num
    
def save_top_videos():
    num_vids  = get_top_num_videos()
    vid_count = 0
    while vid_count < num_vids:
        go_to_highlights()
        video_name = MOVE.NAME_VIDEO
        rpt_seq   = [MOVE.UP, [vid_count, MOVE.RIGHT], MOVE.SPACE, MOVE.SELECT_ALL, MOVE.BACKSPACE, video_name, MOVE.TAB, MOVE.SPACE, [7,MOVE.DOWN], MOVE.LEFT, [7, MOVE.UP], MOVE.TAB, MOVE.SPACE, MOVE.DOWN, MOVE.SPACE]
        perform_sequence(rpt_seq)
        wait_for_video()
        vid_count += 1
    
def save_custom_videos():
    num_vids = get_cus_numtom_videos()
    num_col  = 3
    position = [0,0]
    
    
    stop_position = [num_col - num_vids % num_col, num_vids / num_col]
    reset_seq     = [[3, MOVE.DOWN]]
    video_name    = MOVE.NAME_VIDEO
    
    rpt_seq       = [MOVE.TAB, MOVE.SPACE, MOVE.SELECT_ALL, MOVE.BACKSPACE, video_name, MOVE.TAB, MOVE.SPACE, [7,MOVE.DOWN], MOVE.LEFT, [7, MOVE.UP], MOVE.TAB, MOVE.SPACE, MOVE.DOWN, MOVE.SPACE]
    cur_pos_seq   = [[position[0], MOVE.RIGHT], [position[1], MOVE.DOWN]]
    vid_count     = 0

    
    while vid_count < num_vids:
        vid_count += 1
        
        go_to_highlights()
        perform_sequence(cur_pos_seq)
        perform_sequence(rpt_seq)
        wait_for_video()

        position[0] += 1
        if position[0] > 2:
            position[0] = 0
            position[1] += 1
        cur_pos_seq = [[position[0], MOVE.RIGHT], [position[1], MOVE.DOWN]]
        
def get_config_attribute(attr):
    with open(".\\config.txt", 'r') as f:
        config = f.readlines()
        
    config = [x.strip() for x in config]
    attribute = ""
    
    for line in config:
        if "#" in line:
            continue
            
        if attr in line.lower():
            attribute = line.split('=')[1]
            print "%s is '%s'" % (attr, attribute)
    
    return attribute
    
def get_save_path():
    if config_attributes["save_location"] is None:
        config_attributes["save_location"] = get_config_attribute("save_location")
        
    if os.path.isdir(config_attributes["save_location"]):
        return config_attributes["save_location"]
    else:
        raise FileNotFoundError("Save location not set or path is invalid in config.txt. Path = '%s' does not exist" % config_attributes['save_location'])
    
def get_frame_rate():
    if config_attributes["frame_rate"] is None:
        fr = get_config_attribute("frame_rate")
        try:
            fr = int(fr)
        except:
            raise TypeError("Value for 'frame_rate' is given as '%s' from config.txt. This must be an integer." % fr)
            
        config_attributes["frame_rate"] = fr
        
    return config_attributes["frame_rate"]
    
def trim_videos():
    for file in os.listdir(get_save_path()):
        if ".mp4" in file and "trimmed" not in file:
            ffmpeg_extract_subclip(get_save_path() + file, 5.05, 17.05, targetname=(get_save_path()+ file.replace('.mp4','') + "_trimmed.mp4"))
           
def main():
    if not args.trim_only:
        save_custom_videos()
        save_top_videos()
        
    if not args.no_trim:
        trim_videos()
    
args = parse_user_input()
    
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
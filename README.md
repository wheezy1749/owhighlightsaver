# owHighlightSaver

Got tired of how long it took to save highlights in Overwatch so I made this python script to automatically save them and remove the intro/outtro from the video file. Saves a lot of time for getting clips for highlights.  For anyone that finds this repo and wants to try it out (at your own risk, this takes control of keyboard and mouse movement while the script is running (nothing malicious, have to do keyboard input to save the highlights automatically). You can always end the program with CTRL+C) 

**1) Edit config.txt**

The most you'll have to do in this is change the "save_location". This should be the path that is shown in the Overwatch highlights menu

	save_location=C:\Users\YOUR_USERNAME\Documents\Overwatch\videos\overwatch\

**2) RUN OVERWATCH IN FULLSCREEN WINDOWED MODE!**

Program won't work in any other video mode

**3) Set video settings in game**

Future release will have these options in the config.txt file. However, current patch of OW seems to save all these settings except 'music' for some reason. So I automatically remove the music each time.

**4) Run owhighlightsaver.exe**

Open up cmd or powershell on windows and run with: "owhighlightsaver.exe --h" to get usage options. You'll need to input the number of videos to save for each type of video. Top 5 or custom saves.  

